// Node.Js Routing w/ HTTP Methods - Postman

let http = require("http");


http.createServer((req,res) => {
	if(req.url == "/items" && req.method == "GET"){
		res.writeHead(200,{"Content-Type" : "text/plain"});
		res.end("Data retrieved from database");
	}
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200,{"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database");
	}
}).listen(4000);

console.log("Server now accessible at localhost:4000");


